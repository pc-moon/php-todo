<?php

if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

if($_POST['name']){
    print(
        $_POST['name']
    );
}else{
    print(
        json_encode(
            array(
                ['id'=>1,'name'=> 'First Name'],
                ['id'=>2,'name'=> 'Second Name'],
            )
        )
    );
}